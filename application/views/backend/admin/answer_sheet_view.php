<?php
    $exam_details = $this->crud_model->get_exam_by_id($exam_id)->row_array();
    $course_id=$exam_details['course_id'];
    $question_paper=$exam_details['exam_title'];
    $answer_sheet_path=$exam_details['answer_sheet_path'];
    $course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
?>
<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('Course ').': '.$course_details['title']; ?></h4>
                <h5 class="page-title">  <?php echo get_phrase('Question Paper Title').': '.$exam_details['exam_title']; ?></h5>
                <h5 class="page-title">  <?php echo get_phrase('Answer Sheet of '.$question_paper); ?></h5>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3"><?php echo get_phrase('View Answer Sheet'); ?>
    
                    <a href="<?php echo site_url('admin/exams'); ?>" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm"> <i class=" mdi mdi-keyboard-backspace"></i> <?php echo get_phrase('back_to_question_paper_list'); ?></a>
                </h4>

                <div class="row">
                    <div class="col-xl-12">
                      
                            <div id="basicwizard">
                               

                                
                                    <embed src="<?php echo site_url($answer_sheet_path); ?>" type="application/pdf" width="100%" height="600px" />
                               
                               
                                
                            </div> <!-- end #progressbarwizard-->
                
                     </div>
                </div><!-- end row-->
    </div> <!-- end card-body-->
</div> <!-- end card-->
</div>
</div>

<script type="text/javascript">
$(document).ready(function () {
    initSummerNote(['#description']);
    togglePriceFields('is_free_course');
});
</script>

<script type="text/javascript">
var blank_outcome = jQuery('#blank_outcome_field').html();
var blank_requirement = jQuery('#blank_requirement_field').html();
jQuery(document).ready(function() {
    jQuery('#blank_outcome_field').hide();
    jQuery('#blank_requirement_field').hide();
    calculateDiscountPercentage($('#discounted_price').val());
});

$('.on-hover-action').mouseenter(function() {
    var id = this.id;
    $('#widgets-of-'+id).show();
});
$('.on-hover-action').mouseleave(function() {
    var id = this.id;
    $('#widgets-of-'+id).hide();
});
</script>
