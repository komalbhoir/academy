<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lazyloaddata_syllabus extends CI_Model {

  // constructor
	function __construct()
	{
		parent::__construct();
	}

  // Servre side testing
  function syllables($limit, $start, $col, $dir, $filter_data)
  {
    $this->db->limit($limit,$start);
    $this->db->order_by($col,$dir);

    // apply the filter data
    // check if the user is admin. Admin can not see the draft courses
    if (strtolower($this->session->userdata('role')) == 'admin') {
        $this->db->where("status !=", 'draft');
    }
    if ($filter_data['selected_course_id'] != 'all') {
      $this->db->where('course_id', $filter_data['selected_course_id']);
    }
   
    if ($filter_data['selected_category_id'] != "all") {
      $this->db->where('sub_category_id', $filter_data['selected_category_id']);
    }
  
    if ($filter_data['selected_status'] != "all") {
      $this->db->where('status', $filter_data['selected_status']);
    }
    $query = $this->db->get('syllabus');
    if($query->num_rows() > 0)
    return $query->result();
    else
    return null;

  }

  function syllabus_search($limit, $start, $search, $col, $dir, $filter_data)
  {
    $this->db->like('chapter_title', $search);
    $this->db->limit($limit, $start);
    $this->db->order_by($col, $dir);
    // apply the filter data
    // check if the user is admin. Admin can not see the draft courses
    if (strtolower($this->session->userdata('role')) == 'admin') {
        $this->db->where("status !=", 'draft');
    }
    if ($filter_data['selected_category_id'] != 'all') {
      $this->db->where('sub_category_id', $filter_data['selected_category_id']);
    }
   
    if ($filter_data['selected_status'] != "all") {
      $this->db->where('status', $filter_data['selected_status']);
    }

    $query = $this->db->get('syllabus');
    if($query->num_rows() > 0)
    return $query->result();
    else
    return null;
  }

  function syllabus_search_count($search)
  {
    $query = $this
    ->db
    ->like('chapter_title', $search)
    ->get('syllabus');

    return $query->num_rows();
  }

  function count_all_syllabus($filter_data = array()) {
    // apply the filter data
    // check if the user is admin. Admin can not see the draft courses
    if (strtolower($this->session->userdata('role')) == 'admin') {
        $this->db->where("status !=", 'draft');
    }
    if ($filter_data['selected_course_id'] != 'all') {
      $this->db->where('course_id', $filter_data['selected_course_id']);
    }
    if ($filter_data['selected_category_id'] != "all") {
      $this->db->where('sub_category_id', $filter_data['selected_category_id']);
    }
  
    if ($filter_data['selected_status'] != "all") {
      $this->db->where('status', $filter_data['selected_status']);
    }
    $query = $this->db->get('syllabus');
    return $query->num_rows();
  }
}
