<?php
$syllabus_details = $this->crud_model->get_syllabus_details_by_id($syllabus_id)->row_array();

$course_details=$this->crud_model->get_course();
 
?>

<!-- start page title -->
<div class="row">
  <div class="col-12">
    <div class="page-title-box ">
      <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('update_syllabus'); ?></h4>
    </div>
  </div>
</div>

<div class="row justify-content-md-center">
  <div class="col-xl-6">
    <div class="card">
      <div class="card-body">
        <div class="col-lg-12">
          <h4 class="mb-3 header-title"><?php echo get_phrase('update_syllabus_form'); ?></h4>

          <form class="required-form" action="<?php echo site_url('admin/syllabus_actions/edit/'.$syllabus_id); ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="code"><?php echo get_phrase('course'); ?></label>
              <select class="form-control select2" data-toggle="select2" name="course_id" id="course_id">
                <option value="0"><?php echo get_phrase('none'); ?></option>
                <?php foreach ($course_details->result_array() as $course_dtl): ?>
                  <?php if ($course_dtl['id'] != 0): ?>
                    <option value="<?php echo $course_dtl['id']; ?>" <?php if($syllabus_details['course_id'] == $course_dtl['id']) echo 'selected'; ?>><?php echo $course_dtl['title']; ?></option>
                  <?php endif; ?>
                <?php endforeach; ?>
              </select>
            </div>

            <div class="form-group">
              <label for="name"><?php echo get_phrase('chapter_title'); ?><span class="required">*</span></label>
              <input type="text" class="form-control" id="chapter_title" name = "chapter_title" value="<?php echo $syllabus_details['chapter_title']; ?>" required>
            </div>

            <button type="button" class="btn btn-primary" onclick="checkRequiredFields()"><?php echo get_phrase("submit"); ?></button>
          </form>
        </div>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>

<script type="text/javascript">
function checkCategoryType(category_type) {
  if (category_type > 0) {
    $('#thumbnail-picker-area').hide();
    $('#icon-picker-area').hide();
  }else {
    $('#thumbnail-picker-area').show();
    $('#icon-picker-area').show();
  }
}

$(document).ready(function () {
  var parent_category = $('#parent').val();
  checkCategoryType(parent_category);
});
</script>
