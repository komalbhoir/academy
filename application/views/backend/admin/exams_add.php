<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('add_new_question_paper'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title mb-3"><?php echo get_phrase('question_paper_adding_form'); ?>
                    <a href="<?php echo site_url('admin/exams'); ?>" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm"> <i class=" mdi mdi-keyboard-backspace"></i> <?php echo get_phrase('back_to_question_paper_list'); ?></a>
                </h4>

                <div class="row">
                    <div class="col-xl-12">
                        <form class="required-form" action="<?php echo site_url('admin/exams_actions/add'); ?>" method="post" enctype="multipart/form-data">
                            <div id="basicwizard">

                                


                                <div class="tab-pane" id="chapter">
                                    <div class="row justify-content-center">
                                      <div class="col-xl-8">
                                      
                                        <div class="form-group row mb-3">
                                                    <label class="col-md-4 col-form-label" for="course_title"><?php echo get_phrase('course'); ?> <span class="required">*</span> </label>
                                                    <div class="col-md-10">
                                                    <select class="form-control select2" data-toggle="select2" name="course_id" id="course_id" required>
                                                        <option value=""><?php echo get_phrase('select_a_course'); ?></option>
                                                        <?php $course_list = $this->crud_model->get_courses()->result_array();
                                                            foreach ($course_list as $course):
                                                            if ($course['status'] != 'active')
                                                                continue;?>
                                                            <option value="<?php echo $course['id'] ?>"><?php echo $course['title']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <input type="hidden" name="sub_category_id" id="sub_category_id" value="<?php echo $course['sub_category_id'] ?>">
                                                    </div>
                                        </div>
                                        <div class="form-group row mb-3">
                                                <label class="col-md-4 col-form-label" for="chapter"><?php echo get_phrase('Exam Title'); ?><span class="required">*</span></label>
                                                <div class="col-md-10">
                                                    <div id = "requirement_area">
                                                        <div class="d-flex mt-2">
                                                            <div class="flex-grow-1 px-3">
                                                                <div class="form-group">
                                                                    <input type="text" required class="form-control" name="exam_title" id="exam_title" >
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                  
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        <div class="form-group row mb-3">
                                                <label class="col-md-4 col-form-label" for="chapter"><?php echo get_phrase('Upload_question_paper'); ?><span class="required">*</span></label>
                                                <div class="col-md-10">
                                                    <div id = "requirement_area">
                                                        <div class="d-flex mt-2">
                                                            <div class="flex-grow-1 px-3">
                                                                <div class="form-group">
                                                                    <input type="file" required class="form-control" name="question_paper" id="question_paper" >
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-4 col-form-label" for="chapter"><?php echo get_phrase('Upload_answer_sheet'); ?><span class="required">*</span></label>
                                                <div class="col-md-10">
                                                    <div id = "requirement_area">
                                                        <div class="d-flex mt-2">
                                                            <div class="flex-grow-1 px-3">
                                                                <div class="form-group">
                                                                    <input type="file" required class="form-control" name="answer_sheet" id="answer_sheet" >
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                  
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
 
                               
                                <div class="tab-pane" id="finish">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="text-center">
                                                
                                               
                                                <div class="mb-3 mt-3">
                                                    <button type="button" class="btn btn-primary text-center" onclick="checkRequiredFields()"><?php echo get_phrase('Upload'); ?></button>
                                                </div>
                                            </div>
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </div>

                               

                            </div> <!-- tab-content -->
                        </div> <!-- end #progressbarwizard-->
                    </form>
                </div>
            </div><!-- end row-->
        </div> <!-- end card-body-->
    </div> <!-- end card-->
</div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    initSummerNote(['#description']);
  });
</script>

<script type="text/javascript">
var blank_outcome = jQuery('#blank_outcome_field').html();
var blank_requirement = jQuery('#blank_requirement_field').html();
jQuery(document).ready(function() {
  jQuery('#blank_outcome_field').hide();
  jQuery('#blank_requirement_field').hide();
});
function appendOutcome() {
  jQuery('#outcomes_area').append(blank_outcome);
}
function removeOutcome(outcomeElem) {
  jQuery(outcomeElem).parent().parent().remove();
}

function appendRequirement() {
  jQuery('#requirement_area').append(blank_requirement);
}
function removeRequirement(requirementElem) {
  jQuery(requirementElem).parent().parent().remove();
}

function priceChecked(elem){
  if (jQuery('#discountCheckbox').is(':checked')) {

    jQuery('#discountCheckbox').prop( "checked", false );
  }else {

    jQuery('#discountCheckbox').prop( "checked", true );
  }
}

function topCourseChecked(elem){
  if (jQuery('#isTopCourseCheckbox').is(':checked')) {

    jQuery('#isTopCourseCheckbox').prop( "checked", false );
  }else {

    jQuery('#isTopCourseCheckbox').prop( "checked", true );
  }
}

function isFreeCourseChecked(elem) {

  if (jQuery('#'+elem.id).is(':checked')) {
    $('#price').prop('required',false);
  }else {
    $('#price').prop('required',true);
  }
}

function calculateDiscountPercentage(discounted_price) {
  if (discounted_price > 0) {
    var actualPrice = jQuery('#price').val();
    if ( actualPrice > 0) {
      var reducedPrice = actualPrice - discounted_price;
      var discountedPercentage = (reducedPrice / actualPrice) * 100;
      if (discountedPercentage > 0) {
        jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2)+'%');

      }else {
        jQuery('#discounted_percentage').text('<?php echo '0%'; ?>');
      }
    }
  }
}

</script>

<style media="screen">
body {
  overflow-x: hidden;
}
</style>
